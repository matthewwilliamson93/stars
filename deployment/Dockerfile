# Build Image
#
# A lightweight alpine image to build the static contents for the react app.
FROM alpine:latest as build-stage

# Setup ENV
RUN mkdir /app
WORKDIR /app/

# Install system dependencies
RUN apk add --no-cache --update yarn

# Install NPM dependencies
COPY package.json \
     yarn.lock \
     craco.config.js \
     tsconfig.json \
     rome.json \
     /app/
RUN yarn install

# Build code
COPY public /app/public
COPY src /app/src
RUN yarn build

# Production Image
#
# A NGINX image with custom config and static content from the react app.
FROM nginx:latest

# Setup ENV
EXPOSE 80

# NGINX settings
RUN rm /etc/nginx/conf.d/default.conf
COPY deployment/app.conf /etc/nginx/conf.d

# NGINX static content
COPY --from=build-stage /app/build /usr/share/nginx/html
