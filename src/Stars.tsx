import React from "react";
import styled, { keyframes } from "styled-components";

const tail = (width: number) => keyframes`
    0% {
        width: 0;
    }
    40% {
        width: ${width}px;
    }
    100% {
        width: 0;
    }
`;

const shining = (luminosity: number) => keyframes`
    0% {
        width: 0;
    }
    50% {
        width: ${luminosity}px;
    }
    100% {
        width: 0;
    }
`;

const shooting = (trail: number) => keyframes`
    0% {
        transform: translateX(0);
        opacity: 1;
    }
    70% {
        opacity: 1;
    }
    100% {
        transform: translateX(${trail}px);
        opacity: 0;
    }
`;

const Night = styled.div`
    & {
        align-items: center;
        background: radial-gradient(ellipse at bottom, #1b2735 0%, #090a0f 100%);
        display: flex;
        height: 100vh;
        justify-content: center;
        overflow: hidden;
    }

    height: 100%;
    position: relative;
    width: 100%;
`;

const RotatedDiv = styled.div`
    transform: rotateZ(135deg);
`;

const ShootingStar = styled.div<{
    delay: number;
    height: number;
    left: number;
    luminosity: number;
    opacity: number;
    top: number;
    trail: number;
    width: number;
}>`
    animation:
        ${({ width }) => tail(width)} 3000ms ease-in-out infinite,
        ${({ trail }) => shooting(trail)} 3000ms ease-in-out infinite;
    animation-delay: ${({ delay }) => delay}ms;
    background: linear-gradient(
        -45deg,
        rgba(95, 145, 255, 1),
        rgba(0, 0, 255, 0)
    );
    border-radius: 999px;
    filter: drop-shadow(0 0 6px rgba(105, 155, 255, 1));
    height: 2px;
    left: calc(50% - ${({ left }) => left}px);
    opacity: ${({ opacity }) => opacity};
    position: absolute;
    top: calc(50% - ${({ top }) => top}px);

    &::before {
        height: 2px;
        transform: translateX(50%) rotateZ(45deg);
    }
    &::after {
        height: ${({ height }) => height}px;
        transform: translateX(50%) rotateZ(-45deg);
    }
    &::before,
    &::after {
        animation: ${({ luminosity }) => shining(luminosity)} 3000ms ease-in-out infinite;
        border-radius: 100%;
        background: linear-gradient(
            -45deg,
            rgba(0, 0, 255, 0),
            rgba(95, 145, 255, 1),
            rgba(0, 0, 255, 0)
        );
        content: '';
        position: absolute;
        right: 0;
        top: calc(50% - 1px);
    }
`;

export default function Stars(): JSX.Element {
    return (
        <Night>
            <RotatedDiv>
                {Array.from({ length: 42 }, (_, index) => (
                    <ShootingStar
                        delay={Math.random() * 10000}
                        width={Math.random() * 100 + 100}
                        height={Math.random() * 2 + 2}
                        left={Math.random() * 300}
                        luminosity={Math.random() * 20 + 20}
                        opacity={(Math.random() * 50) / 100 + 0.5}
                        top={Math.random() * 400 - 200}
                        trail={Math.random() * 100 + 300}
                        key={`shooting-star-${index}`}
                    />
                ))}
            </RotatedDiv>
        </Night>
    );
}
