import React from "react";

import Stars from "./Stars";

/*
 * A React component for the app.
 */
export default function App(): JSX.Element {
    return <Stars />;
}
