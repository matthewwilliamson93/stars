const CracoSwcPlugin = require("craco-swc");

module.exports = {
    plugins: [
        {
            plugin: CracoSwcPlugin,
            options: {
                swcLoaderOptions: {
                    minify: false,
                    jsc: {
                        minify: {
                            compress: false,
                        },
                        target: "es2015",
                        parser: {
                            syntax: "typescript",
                            jsx: true,
                            dynamicImport: true,
                            exportDefaultFrom: true,
                        },
                    },
                },
            },
        },
    ],
};
